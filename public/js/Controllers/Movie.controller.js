angular.module('BoredCinephile').controller('MoviesController', MoviesController);

function MoviesController ($scope) {
  $scope.getImageUrl = (movie) => 
    `//image.tmdb.org/t/p/w300${movie.poster_path}`
}
