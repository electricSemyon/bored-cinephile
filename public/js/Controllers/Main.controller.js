angular.module('BoredCinephile', ['ngMaterial'])
  .controller('MainController', function ($scope, $mdSidenav, $timeout, $http, $mdDialog, MoviesService) {
    $scope.APP_TITLE = 'Bored Cinephile';

    const buildToggler = (componentId) => () => $mdSidenav(componentId).toggle();

    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    $scope.catList = [
      { "id": 28,     "name": "Action" },
      { "id": 12,     "name": "Adventure" },
      { "id": 16,     "name": "Animation" },
      { "id": 35,     "name": "Comedy" },
      { "id": 80,     "name": "Crime" },
      { "id": 99,     "name": "Documentary" },
      { "id": 18,     "name": "Drama" },
      { "id": 10751,  "name": "Family" },
      { "id": 14,     "name": "Fantasy" },
      { "id": 36,     "name": "History" },
      { "id": 27,     "name": "Horror" },
      { "id": 10402,  "name": "Music" },
      { "id": 9648,   "name": "Mystery" },
      { "id": 10749,  "name": "Romance" },
      { "id": 878,    "name": "Science Fiction" },
      { "id": 10770,  "name": "TV Movie" },
      { "id": 53,     "name": "Thriller" },
      { "id": 10752,  "name": "War" },
      { "id": 37,     "name": "Western" }
    ];

    $scope.showModal = () => $mdDialog
      .show($mdDialog
        .alert()
        .title('About me')
        .textContent(`Меня зовут Слава, и я веб-разработчик. 
            Это приложение я написал в воскресенье за пол дня на
            малоизвестных мне технологиях (AngularJS, MaterialUI). На английском оно
            ввиду того, что, быть может я когда-то опубликую его на своём
            github\`е. Тут нет многого из того,
            что я хотел бы реализовать (livesearch, пагинация или инфинит скролл). Зато, кстати,
            хоть я этого и не планировал, но сайт получился полностью адаптивным для всех устройств.
            В любом случае, спасибо themoviedb за предоставленное халявное api. 
            Из него я и беру всю эту информацию о фильмах. Из-за спешки,
            под капотом это всё выглядит ну очень так себе. Так случилось потому
            что у меня много работы. Надеюсь на ваше понимание. Upd: я порефакторил раскидал 
            код по контроллерам и сервисам, и теперь уже повеселее вроде выглядит.`)
        .ariaLabel('More info')
        .ok('Got it')
      );

    $scope.getMoviesList = (genreId) => {
      $scope.currentGenre = $scope.catList.find(genre => genre.id === genreId).name;

      const randomPage = Math.floor(Math.random() * 20);

      MoviesService.getMoviesByGenreId(genreId, randomPage)
      .then(res => {
        console.log(res);
        $scope.movies = res.data.results
      })
    }
  })
  .config(['$mdIconProvider', function($mdIconProvider) {
    $mdIconProvider
      .icon('menu', '../../img/menu') 
      .icon('close', '../../img/close')
  }])
