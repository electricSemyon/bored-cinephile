angular.module('BoredCinephile').factory('MoviesService', MoviesService);

function MoviesService ($http) {
  const getMoviesByGenreId = (genreId, page) => {
    return $http.get(`//api.themoviedb.org/3/discover/movie?api_key=72547e800d7d9809528afdd38877ebb0&language=en-US
        &sort_by=popularity.desc
        &include_adult=true
        &include_video=false
        &page=${page}&with_genres=${genreId}`)
  }

  return {
    getMoviesByGenreId
  }
}
