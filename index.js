const fs = require('fs');
const path = require('path');
const http = require('http');
const express = require('express');
const app = express();
const port = process.env.PORT || 80;

app.use(express.static('public'));
app.use('/img', express.static(path.join(__dirname, 'public/img')));

const httpServer = http.createServer(app);

httpServer.listen(port, () => console.log(`server listens on port ${port}`));
